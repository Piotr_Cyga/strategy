package strategies;

import java.util.Random;

/**
 * Created by amen on 8/17/17.
 */
public class RandomStrategy implements IInputStrategy {
    private Random rd = new Random();

    @Override
    public int getInt() {
        return rd.nextInt();
    }

    @Override
    public String getString() {
        int zakres = rd.nextInt(28)+4;
        String returnedString = null;
        char [] tablicaCharow = new char[zakres];
        for (int i =0; i<zakres; i++) {
            tablicaCharow [i] = (char) (rd.nextInt(25) + 97);

        }
        tablicaCharow[0] = Character.toUpperCase(tablicaCharow [0]);
        returnedString=String.valueOf(tablicaCharow);
        return returnedString;
    }

    @Override
    public double getDouble() {
    return rd.nextDouble();
    }
}
