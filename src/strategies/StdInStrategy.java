package strategies;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by amen on 8/17/17.
 */
public class StdInStrategy implements IInputStrategy {
    private Scanner sc;

    public StdInStrategy() {
        sc = new Scanner(System.in);
    }

    @Override
    public int getInt() {
        System.out.println("podaj inta");
        return sc.nextInt();
    }

    @Override
    public String getString() {
        System.out.println("podaj stringa");
        return sc.nextLine();
    }

    @Override
    public double getDouble() {
        System.out.println("podaj doubla");
        return sc.nextDouble();
    }
}
