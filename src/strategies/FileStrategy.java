package strategies;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

/**
 * Created by amen on 8/17/17.
 */
public class FileStrategy implements IInputStrategy {
    private Scanner sc;

    public FileStrategy() {
        File file = new File("C:\\Users\\user\\Projekty\\overview\\src\\input.txt");
        try {
            sc = new Scanner(new FileReader (file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getInt() {
        if (sc.hasNextInt()) {
            return sc.nextInt();
        }
        throw new NullPointerException();
    }

    @Override
    public String getString() {
        if (sc.hasNext()) {
            return sc.nextLine();
        }
        throw new NullPointerException();
    }

    @Override
    public double getDouble() {
        if (sc.hasNextDouble()) {
            return sc.nextDouble();
        }
        throw new NullPointerException();
    }
}
